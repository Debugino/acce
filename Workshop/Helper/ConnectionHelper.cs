﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Workshop.XlmsService;
using WorkshopModels;

namespace Workshop.Helper
{
    public class ConnectionHelper
    {
        public TicketHeader _ticketHeader;
        public XLMSSoapClient _xlms;
        private string XUNIQUEKEY;
        private string DBID = ConfigurationManager.AppSettings["DBID"];
        private string APPSRVID = ConfigurationManager.AppSettings["AppSrvID"];
        private int SESSIONTIMEOUT = Convert.ToInt32(ConfigurationManager.AppSettings["SessionTimeout"]);
        private string AdminUserName = ConfigurationManager.AppSettings["AdminUserName"];
        private string AdminPassword = ConfigurationManager.AppSettings["AdminPassword"];
        private int AdminAuthType = Convert.ToInt32(ConfigurationManager.AppSettings["AdminAuthType"]);

        public ConnectionHelper()
        {
            _xlms = new XLMSSoapClient();
        }

        public bool Connect(string username, string password, string xlocator)
        {
            //TODO : verificatin on database
            var userValid = new DataHelper().ValidateUserInfo(username, password);

            if (userValid)
            {
                _ticketHeader = new TicketHeader();
                _ticketHeader.DBID = DBID;
                _ticketHeader.UserName = AdminUserName;
                _ticketHeader.Password = AdminPassword;
                _ticketHeader.AuthType = AdminAuthType == 0 ? AuthenticationType.ADMIN : AuthenticationType.STUDENT;
                //_ticketHeader.AuthType = AuthenticationType.STUDENT;
                //_ticketHeader.AppSvrID = APPSRVID;
                //_ticketHeader.SessionTimeout = SESSIONTIMEOUT;
                try
                {
                    _xlms.Connect(ref _ticketHeader);
                    return AddToClass(username, xlocator);
                }
                catch (Exception ex)
                {
                    return false;
                }

            }
            return false;          
                        
        }

        public bool TestConnect(string username, string password)
        {
            try
            {
                _ticketHeader = new TicketHeader();
                _ticketHeader.DBID = DBID;
                _ticketHeader.UserName = username;
                _ticketHeader.Password = password;
                _ticketHeader.AuthType = AdminAuthType == 0 ? AuthenticationType.ADMIN : AuthenticationType.STUDENT;

                _xlms.Connect(ref _ticketHeader);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool AddToClass(string xuniquekey, string xlocator)
        {
            RegistrationData testRegistration = new RegistrationData();
            Field[] aFields = new Field[3];
            Field f = new Field();
            f.Name = "XLOCATOR";
            f.Value = xlocator;
            aFields[0] = f;
            f = new Field();
            f.Name = "XUNIQUEKEY";
            f.Value = xuniquekey;
            aFields[1] = f;
            f = new Field();
            f.Name = "XSTATUS";
            f.Value = "E";
            aFields[2] = f;
            testRegistration.IDDataFields = aFields;

            RegisteringOptions options = new RegisteringOptions();
            options.createNew = Choice.DONTCARE;
            options.ignoreNone = true;

            try
            {
                var key = _xlms.Register(ref _ticketHeader, testRegistration, options);
                Disconnect();
                return true;
            }
            catch (Exception ex)
            {
                Disconnect();
                return false;
            }
        }


        public bool CreatePerson(UserViewModel usr)
        {
            try
            {
                _ticketHeader = new TicketHeader();
                _ticketHeader.DBID = DBID;
                _ticketHeader.UserName = AdminUserName;
                _ticketHeader.Password = AdminPassword;

                _ticketHeader.AuthType = AdminAuthType == 0 ? AuthenticationType.ADMIN : AuthenticationType.STUDENT;
                _xlms.Connect(ref _ticketHeader);
                var personData = InitializePersonData(usr);
                
                var xuniquekey = _xlms.CreatePerson(ref _ticketHeader, personData);

                if (!string.IsNullOrEmpty(xuniquekey))
                {
                    return AddToClass(xuniquekey, usr.Locator);
                }
                return false;
            }
            catch (Exception ex)
            {
                Disconnect();
                return false;
            }
        }

        private PersonData InitializePersonData(UserViewModel usr)
        {
            var personData = new PersonData();

           // Field[] aFields = new Field[19];

            var af = new List<Field>();

            // For the list of valid field names consult System Manager : Fields : Person tab
            Field f = new Field();
            f.Name = "XUNIQUEKEY";
            f.Value = usr.UniqueKey;
            if (!string.IsNullOrEmpty(f.Value))
                //aFields[0] = f;
                af.Add(f);

           f = new Field();
            f.Name = "XLASTNAME";
            f.Value = usr.LastName;
            if (!string.IsNullOrEmpty(f.Value))
                //aFields[1] = f;
                af.Add(f);

            f = new Field();
            f.Name = "XFIRSTNAME";
            f.Value = usr.FirstName;
            if (!string.IsNullOrEmpty(f.Value))
                //aFields[2] = f;
                af.Add(f);


            f = new Field();
            f.Name = "UPASSWORD";
            f.Value = usr.Password;
            if (!string.IsNullOrEmpty(f.Value))
                //aFields[3] = f;
                af.Add(f);

            //f = new Field();
            //f.Name = "XPASSWORD";
            //f.Value = usr.Password;
            //if (!string.IsNullOrEmpty(f.Value))
            //    //aFields[4] = f;
            //    af.Add(f);

            f = new Field();
            f.Name = "XEMAIL";
            f.Value = usr.Email;
            if (!string.IsNullOrEmpty(f.Value))
                // aFields[5] = f;
                af.Add(f);

            f = new Field();
            f.Name = "XUSER";
            f.Value = "CROESSET";
            if (!string.IsNullOrEmpty(f.Value))
                // aFields[6] = f;
                af.Add(f);

            f = new Field();
            f.Name = "ADDRESS_1";
            f.Value = usr.Address1;
            if (!string.IsNullOrEmpty(f.Value))
                // aFields[7] = f;
                af.Add(f);


            f = new Field();
            f.Name = "AIALIC";
 
            f.Value = usr.AIA;
            if (!string.IsNullOrEmpty(f.Value))
                //aFields[8] = f;
                af.Add(f);


            f = new Field();
            f.Name = "AIBDID";
            f.Value = usr.AIBD;
            if (!string.IsNullOrEmpty(f.Value))
                //aFields[9] = f;
                af.Add(f);


            f = new Field();
            f.Name = "CITY";
            f.Value = usr.City;
            if (!string.IsNullOrEmpty(f.Value))
                //aFields[10] = f;
                af.Add(f);


            f = new Field();
            f.Name = "FLID";
            f.Value = usr.FLID;
            if (!string.IsNullOrEmpty(f.Value))
                // aFields[11] = f;
                af.Add(f);


            f = new Field();
            f.Name = "ICCID";
            f.Value = usr.ICC;
            if (!string.IsNullOrEmpty(f.Value))
                //aFields[12] = f;
                af.Add(f);

            f = new Field();
            f.Name = "LICNUMBER";
            f.Value = usr.LicenceNumber;
            if (!string.IsNullOrEmpty(f.Value))
                //aFields[13] = f;
                af.Add(f);

            f = new Field();
            f.Name = "NYID";
            f.Value = usr.NYID;
            if (!string.IsNullOrEmpty(f.Value))
                //aFields[14] = f;
                af.Add(f);

            f = new Field();
            f.Name = "CUSTTYPE";
            f.Value = usr.Occupation;
            if (!string.IsNullOrEmpty(f.Value))
                //aFields[15] = f;
                af.Add(f);

            f = new Field();
            f.Name = "PHONE";
            f.Value = usr.Phone;
            if (!string.IsNullOrEmpty(f.Value))
                //aFields[16] = f;
                af.Add(f);

            f = new Field();
            f.Name = "STATE";
            f.Value = usr.State;
            if (!string.IsNullOrEmpty(f.Value))
                //aFields[17] = f;
                af.Add(f);

            f = new Field();
            f.Name = "AZIPC";
            f.Value = usr.ZIPCode;
            if (!string.IsNullOrEmpty(f.Value))
                // aFields[18] = f;
                af.Add(f);

            personData.IDDataFields = af.ToArray();



            return personData;
        }


        public void SetAdminTicketHeader()
        {
            _ticketHeader = new TicketHeader();
            _ticketHeader.DBID = "custdev";
            _ticketHeader.UserName = "AJAY";
            _ticketHeader.Password = null;            
            _ticketHeader.AuthType = AuthenticationType.ADMIN;
            //_ticketHeader.SessionTimeout = SESSIONTIMEOUT;
        }

        public bool Disconnect()
        {
            try
            {
                _xlms.Disconnect(ref _ticketHeader);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        
    }
}