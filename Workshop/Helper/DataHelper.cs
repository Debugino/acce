﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WorkshopModels;

namespace Workshop.Helper
{
    public class DataHelper
    {
        private SqlConnection con;

        private void connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["getconn"].ToString();
            con = new SqlConnection(constr);
        }

        private void OpenConnection()
        {
            connection();
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
        }

        private void CloseConnection()
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }

        public string GetUniqueKey(string username)
        {
            SqlCommand com = new SqlCommand("select xuniquekey from pathlore_data.person where xuniquekey='" + username +"'", con);
            com.CommandType = CommandType.Text;
            OpenConnection();
            com.Connection = con;
            var xuniquekey = "";
            var reader = com.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    xuniquekey = reader["xuniquekey"].ToString();
                }
            }
            reader.Close();
            CloseConnection();
            return xuniquekey;
        }

        public bool ValidateUserInfo(string username, string password)
        {
            SqlCommand cmd = new SqlCommand("select Count(*) from pathlore_data.person where xuniquekey='" + username + "' and upassword='" + password +"'");
            cmd.CommandType = CommandType.Text;
            OpenConnection();
            cmd.Connection = con;
            try
            {
                var count = Convert.ToInt32(cmd.ExecuteScalar());
                if (count == 1)
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool ValidateUserName(string username)
        {
            SqlCommand cmd = new SqlCommand("select Count(*) from pathlore_data.person where xuniquekey='" + username + "'");
            cmd.CommandType = CommandType.Text;
            OpenConnection();
            cmd.Connection = con;
            try
            {
                var count = Convert.ToInt32(cmd.ExecuteScalar());
                if (count > 0)
                {
                    return false;
                }
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public List<Dictionary<string, string>> GetOccupations()
        {
            var occupations = new List<Dictionary<string, string>>();
            SqlCommand com = new SqlCommand("select col1, col2 from pathlore_data.lut_person_custtype", con);
            com.CommandType = CommandType.Text;
            OpenConnection();       
            com.Connection = con;

            try
            {
                var reader = com.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var dictionnary = new Dictionary<string, string>();
                        dictionnary.Add(reader["col1"].ToString(), reader["col2"].ToString());
                        occupations.Add(dictionnary);
                    }
                }
                reader.Close();
                CloseConnection();
            }
            catch (Exception)
            {
                CloseConnection();
            }
            
            return occupations;
        }

        public ClassInfoViewModel GetClassInfo(string locator)
        {
            var classInfoVm = new ClassInfoViewModel();
            SqlCommand com = new SqlCommand("pathlore_data.sbs_sp_getClassInfoFromXlocator", con);
            com.Parameters.AddWithValue("@xlocator", locator);
            com.CommandType = CommandType.StoredProcedure;
            OpenConnection();
            com.Connection = con;
            classInfoVm.XLocator = locator;
            try
            {
                var reader = com.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        classInfoVm.ClassName = reader.GetString(0);
                        classInfoVm.ClassStartDate = reader.GetDateTime(1).ToShortDateString();
                        //var dictionnary = new Dictionary<string, string>();
                        //dictionnary.Add(reader["col1"].ToString(), reader["col2"].ToString());
                        //occupations.Add(dictionnary);
                    }
                }
                reader.Close();
                CloseConnection();
            }
            catch (Exception)
            {
                CloseConnection();
            }

            return classInfoVm;
           
        }
        public List<LoginViewModel> GetUsers()
        {
            var result = new List<LoginViewModel>();
            SqlCommand com = new SqlCommand("select xuniquekey, upassword from pathlore_data.person", con);
            com.CommandType = CommandType.Text;
            OpenConnection();
            com.Connection = con;
            try
            {
                var reader = com.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result.Add(new LoginViewModel() { Username = reader["col1"].ToString(), Password =  reader["col2"].ToString()});
                    }
                }
                reader.Close();
                CloseConnection();
            }
            catch (Exception)
            {
                CloseConnection();
            }
            return result;
        }

    }

    
}