﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SoapHttpClient;
using WorkshopModels;
using System.Configuration;
using System.Xml.Linq;
using System.Web.Http;
using SoapHttpClient.Extensions;
using System.Xml.Serialization;
using System.Xml;
using System.Net;
using System.Text;
using System.IO;

namespace Workshop.Helper
{    
    public static class SoapHelper
    {
        //private static string SOAPNamespace = ConfigurationManager.AppSettings["SoapNamespace"];


        //public static string RunAction(RequestObjectFromat requestObjectFromat, TicketHeader ticketHeader, string endpoint, object obj)
        //{
            
        //    var soapTicketHeader = CreateTicketHeader(ticketHeader);
        //    var soapEnvelopeXml = CreateSoapEnvelope(soapTicketHeader, requestObjectFromat);
        //    var request = CreateWebRequest(endpoint, requestObjectFromat.Action);
        //    InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, request);
        //    // begin async call to web request.
        //    IAsyncResult asyncResult = request.BeginGetResponse(null, null);

        //    // suspend this thread until call is complete. You might want to
        //    // do something usefull here like update your UI.
        //    asyncResult.AsyncWaitHandle.WaitOne();
            
        //    // get the response from the completed web request.
        //    string soapResult;
        //    try
        //    {
        //        using (WebResponse webResponse = request.EndGetResponse(asyncResult))
        //        {
        //            using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
        //            {
        //                soapResult = rd.ReadToEnd();
        //            }
        //            return soapResult;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        return "";
        //    }
           
        //    //return null;
        //}

        //private static HttpWebRequest CreateWebRequest(string url, string action)
        //{
        //    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
        //    webRequest.Headers.Add("SOAPAction", action);
        //    webRequest.ContentType = "text/xml;charset=\"utf-8\"";
        //    webRequest.Accept = "text/xml";
        //    webRequest.Method = "POST";
            
        //    return webRequest;
        //}

        //private static string CreateTicketHeader(TicketHeader ticketHeader)
        //{
        //    //r = string.Format(@"<TicketHeader xmlns=""{0}"">                                      <A
        //    //                    </TicketHeader>", SOAPNamespace);
        //    var headerXml = string.Format(
        //                @"<TicketHeader xmlns = ""{0}""> 
        //                        <AuthType>{1}</AuthType>
        //                        <SessionTimeout>{2}</SessionTimeout>
        //                        <DBID>{3}</DBID>
        //                        <UserName>{4}</UserName> 
        //                        <Password>{5}</Password>
        //                        <SiteID>{6}</SiteID>
        //                        <ConnectID>{7}</ConnectID>
        //                        <AppSvrID>{8}</AppSvrID>
        //                </TicketHeader >",
        //                "http://www.pathlore.com/xlms/", 
        //                ticketHeader.AuthType,
        //                ticketHeader.SessionTimeout,
        //                ticketHeader.DBID,
        //                ticketHeader.UserName,
        //                ticketHeader.Password,
        //                ticketHeader.SiteID,
        //                ticketHeader.ConnectID,
        //                ticketHeader.AppSvrID);
     
        //    return headerXml;
        //}

        //private static string CreateBody(TicketHeader ticketHeader)
        //{
        //    //r = string.Format(@"<TicketHeader xmlns=""{0}"">                                      <A
        //    //                    </TicketHeader>", SOAPNamespace);
        //    var headerXml = string.Format(
        //                @"<TicketHeader xmlns = ""{0}""> 
        //                        <AuthType>{1}</AuthType>
        //                        <SessionTimeout>{2}</SessionTimeout>
        //                        <DBID>{3}</DBID>
        //                        <UserName>{4}</UserName> 
        //                        <Password>{5}</Password>
        //                        <SiteID>{6}</SiteID>
        //                        <ConnectID>{7}</ConnectID>
        //                        <AppSvrID>{8}</AppSvrID>
        //                </TicketHeader >",
        //                SOAPNamespace,
        //                ticketHeader.AuthType,
        //                ticketHeader.SessionTimeout,
        //                ticketHeader.DBID,
        //                ticketHeader.UserName,
        //                ticketHeader.Password,
        //                ticketHeader.SiteID,
        //                ticketHeader.ConnectID,
        //                ticketHeader.AppSvrID);

        //    return headerXml;
        //}

        //public static string ReadRespondField(string responseSOAP, string name)
        //{
        //    XDocument xDocc = XDocument.Parse(responseSOAP);
        //    XmlReader xr = xDocc.CreateReader();
        //    xr.ReadToFollowing(name);
        //    string value = xr.ReadElementString();
        //    return value;
        //}
        //private static XmlDocument CreateSoapEnvelope(string ticketHeader, RequestObjectFromat requestObjectFromat)
        //{
        //    XmlDocument soapEnvelop = new XmlDocument();
        //    var envelop = string.Format(
        //        @"<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
        //            <soap:Header>
        //                {0}
        //            </soap:Header>
        //            <soap:Body>
        //                {1}
        //            </soap:Body>
        //        </soap:Envelope>", ticketHeader, requestObjectFromat.Envelop);

        //    soapEnvelop.LoadXml(envelop);


        //    return soapEnvelop;
        //}
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="soapEnvelopeXml"></param>
        ///// <param name="webRequest"></param>
        //private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        //{
        //    var data = new UTF8Encoding().GetBytes(soapEnvelopeXml.InnerXml.Replace("\\", ""));
        //    using (Stream stream = webRequest.GetRequestStream())
        //    {
        //        //soapEnvelopeXml.Save(stream);
        //        stream.Write(data, 0, data.Length);
        //    }
        //}
    }
}