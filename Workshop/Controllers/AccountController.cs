﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Workshop.Helper;
using WorkshopModels;

namespace Workshop.Controllers
{
    public class AccountController : Controller
    {
        private ConnectionHelper _conn;
        

        // GET: Account
        public ActionResult Login(string locator)
        {
            //ViewBag.Locator = "0000003132"; //just for testing

            if (string.IsNullOrEmpty(locator))
            {
                return View("InvalidLocator");
            }

            var classInfo = new DataHelper().GetClassInfo(locator);
            if (string.IsNullOrEmpty(classInfo.ClassName))
            {
                return View("InvalidLocator");
            }

            ViewBag.Locator = locator;
            ViewBag.States = GetStates();
            ViewBag.Occupations = GetOccupations();
            ViewBag.ClassInfos = classInfo;

            return View();
        }

        
        // POST: Account/Create
        [HttpPost]
        public ActionResult Login(LoginViewModel lvm)
        {
            ClearTempData();
            var locator = lvm.Locator ;
            try
            {
                if (!string.IsNullOrEmpty(lvm.Username) && !string.IsNullOrEmpty(lvm.Locator))
                {
                    _conn = new ConnectionHelper();
                    var connectOp = _conn.Connect(lvm.Username, lvm.Password, lvm.Locator);
                    if (!connectOp)
                    {
                        TempData["ShowError"] = true;
                        return RedirectToAction("Login", new { locator = lvm.Locator });
                    }
                    else
                    {
                        ViewBag.ClassInfos = new DataHelper().GetClassInfo(locator);
                        return View("UserAddedToClassSuccess");
                    }
                }
                TempData["InvalidLocator"] = true;
                return RedirectToAction("Login", new { locator = lvm.Locator });
                
            }
            catch
            {
                TempData["SomethingHappened"] = true;
                return RedirectToAction("Login", new { locator = lvm.Locator });
            }
            
        }

        public List<SelectListItem> GetOccupations()
        {
            var occups = new DataHelper().GetOccupations();
            var result = new List<SelectListItem>();
            result.Add(new SelectListItem { Text = "Please select", Value = "" });
            foreach (var item in occups)
            {
                result.Add(new SelectListItem { Value = item.Keys.FirstOrDefault(), Text = item.Values.FirstOrDefault() });
            }
            return result;
        }

        private List<SelectListItem> GetStates()
        {
            var states = new List<SelectListItem>();
            states.Add(new SelectListItem { Value = "", Text = "Please select" });
            states.Add(new SelectListItem { Value = "AL", Text = "Alabama" });
            states.Add(new SelectListItem{Value = "AK", Text = "Alaska"});
            states.Add(new SelectListItem{Value = "AZ", Text = "Arizona"});
            states.Add(new SelectListItem{Value = "AR", Text = "Arkansas"});
            states.Add(new SelectListItem{Value = "CA", Text = "California"});
            states.Add(new SelectListItem{Value = "CO", Text = "Colorado"});
            states.Add(new SelectListItem{Value = "CT", Text = "Connecticut"});
            states.Add(new SelectListItem{Value = "DE", Text = "Delaware"});
            states.Add(new SelectListItem{Value = "DC", Text = "District Of Columbia"});
            states.Add(new SelectListItem{Value = "FL", Text = "Florida"});
            states.Add(new SelectListItem{Value = "GA", Text = "Georgia"});
            states.Add(new SelectListItem{Value = "HI", Text = "Hawaii"});
            states.Add(new SelectListItem{Value = "ID", Text = "Idaho"});
            states.Add(new SelectListItem{Value = "IL", Text = "Illinois"});
            states.Add(new SelectListItem{Value = "IN", Text = "Indiana"});
            states.Add(new SelectListItem{Value = "IA", Text = "Iowa"});
            states.Add(new SelectListItem{Value = "KS", Text = "Kansas"});
            states.Add(new SelectListItem{Value = "KY", Text = "Kentucky"});
            states.Add(new SelectListItem{Value = "LA", Text = "Louisiana"});
            states.Add(new SelectListItem{Value = "ME", Text = "Maine"});
            states.Add(new SelectListItem{Value = "MD", Text = "Maryland"});
            states.Add(new SelectListItem{Value = "MA", Text = "Massachusetts"});
            states.Add(new SelectListItem{Value = "MI", Text = "Michigan"});
            states.Add(new SelectListItem{Value = "MN", Text = "Minnesota"});
            states.Add(new SelectListItem{Value = "MS", Text = "Mississippi"});
            states.Add(new SelectListItem{Value = "MO", Text = "Missouri"});
            states.Add(new SelectListItem{Value = "MT", Text = "Montana"});
            states.Add(new SelectListItem{Value = "NE", Text = "Nebraska"});
            states.Add(new SelectListItem{Value = "NV", Text = "Nevada"});
            states.Add(new SelectListItem{Value = "NH", Text = "New Hampshire"});
            states.Add(new SelectListItem{Value = "NJ", Text = "New Jersey"});
            states.Add(new SelectListItem{Value = "NM", Text = "New Mexico"});
            states.Add(new SelectListItem{Value = "NY", Text = "New York"});
            states.Add(new SelectListItem{Value = "NC", Text = "North Carolina"});
            states.Add(new SelectListItem{Value = "ND", Text = "North Dakota"});
            states.Add(new SelectListItem{Value = "OH", Text = "Ohio"});
            states.Add(new SelectListItem{Value = "OK", Text = "Oklahoma"});
            states.Add(new SelectListItem{Value = "OR", Text = "Oregon"});
            states.Add(new SelectListItem{Value = "PA", Text = "Pennsylvania"});
            states.Add(new SelectListItem{Value = "RI", Text = "Rhode Island"});
            states.Add(new SelectListItem{Value = "SC", Text = "South Carolina"});
            states.Add(new SelectListItem{Value = "SD", Text = "South Dakota"});
            states.Add(new SelectListItem{Value = "TN", Text = "Tennessee"});
            states.Add(new SelectListItem{Value = "TX", Text = "Texas"});
            states.Add(new SelectListItem{Value = "UT", Text = "Utah" });
            states.Add(new SelectListItem{Value = "VT", Text = "Vermont" });
            states.Add(new SelectListItem{Value = "VA", Text = "Virginia" });
            states.Add(new SelectListItem{Value = "WA", Text = "Washington" });
            states.Add(new SelectListItem{Value = "WV", Text = "West Virginia" });
            states.Add(new SelectListItem{Value = "WI", Text = "Wisconsin" });
            states.Add(new SelectListItem { Value = "WY", Text = "Wyoming" });
            return states;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userVm"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Register(UserViewModel userVm)
        {
            var locator = userVm.Locator;
            ClearTempData();            
            try
            {
                if (!string.IsNullOrEmpty(userVm.UniqueKey) )
                {
                    var validateUser = new DataHelper().ValidateUserName(userVm.UniqueKey);

                    if (!validateUser)
                    {
                        TempData["UserNameExist"] = true;
                        return RedirectToAction("Login", new { locator = locator });
                    }

                    _conn = new ConnectionHelper();

                    var created = _conn.CreatePerson(userVm);

                    if (created)
                    {
                        ViewBag.ClassInfos = new DataHelper().GetClassInfo(locator);
                        return View("UserCreatedSuccess");
                    }
                    TempData["SomethingHappened"] = true;
                    return RedirectToAction("Login", new { locator = locator });
                }
                TempData["SomethingHappened"] = true;
                return RedirectToAction("Login", new { locator = locator });
            }
            catch (Exception ex)
            {
                TempData["SomethingHappened"] = true;
                return RedirectToAction("Login", new { locator = locator });
            }
            
        }

        public ActionResult ValidateUsername(string username)
        {
            var data = new DataHelper();
            if (!string.IsNullOrEmpty(data.GetUniqueKey(username)))
            {
                return Json(new { exist = true });
            }

            return Json(new { exist = false });
        }

        [HttpGet]
        public ActionResult ResetPasswword()
        {
            var resetUrl = ConfigurationManager.AppSettings["ForgotPWUrl"];
            return Json(new { url = resetUrl }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ForgotPassword()
        {
            ViewBag.ForgotUrl = ConfigurationManager.AppSettings["ForgotPWUrl"];
            return View();
        }

        private void ClearTempData()
        {
            TempData["ShowError"] = null;
            TempData["SomethingHappened"] = null;
            TempData["UserNameExist"] = null;
        }

        /// <summary>
        /// Tests the connect.
        /// </summary>
        /// <returns>System.Web.Mvc.ActionResult.</returns>
        public ActionResult TestConnect()
        {
            var users = new DataHelper().GetUsers();
            foreach (var usr in users)
            {
                _conn = new ConnectionHelper();
                if (_conn.TestConnect(usr.Username, usr.Password))
                {
                    return Json(new {usr.Username, usr.Password });
                }
            }

            return Json(new { Username = "", Password = "" });
        }


    }
}
