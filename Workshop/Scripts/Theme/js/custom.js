﻿
/*
* INTERNET EXPLORER CHECK FUNCTION
*
*/

$(document).ready(function ($) {
    msieversion(".btn");
});
function msieversion(itemClassToDisabled) {
    var $el = $('' + itemClassToDisabled + '');
    var $mainContainer = $('#main-container');
    var $msg = "<div class='alert alert-warning alert-dismissible' role='alert' style='margin-top: 10px;'>" +
                "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
                "<span aria-hidden='true'>&times;</span></button>" +
                "<strong>Warning!</strong> This application is not supported by this browser. Please use Google Chrome." +
                "</div>";
    
    if (navigator.userAgent.indexOf('MSIE') != -1)
        var detectIEregexp = /MSIE (\d+\.\d+);/ //test for MSIE x.x
    else // if no "MSIE" string in userAgent
        var detectIEregexp = /Trident.*rv[ :]*(\d+\.\d+)/ //test for rv:x.x or rv x.x where Trident string exists

    if (detectIEregexp.test(navigator.userAgent)) { //if some form of IE
        $el.addClass("disabled");
        $mainContainer.prepend($msg);
        var ieversion = new Number(RegExp.$1) // capture x.x portion and store as a number
        //if (ieversion >= 12)
            //document.write("You're using IE12 or above")
    }
    //else {
    //    document.write("n/a")
    //    $el.addClass("disabled");
    //}
    
    
}

