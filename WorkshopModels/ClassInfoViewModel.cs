﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkshopModels
{
    public class ClassInfoViewModel
    {
        public string XLocator { get; set; }
        public string ClassName { get; set; }
        public string ClassStartDate { get; set; }

    }
}
