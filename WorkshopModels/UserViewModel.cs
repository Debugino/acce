﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkshopModels
{
    public class UserViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Locator { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public string UniqueKey { get; set; }
        public string LicenceNumber { get; set; }
        public string Address1 { get; set; }
        public string ZIPCode { get; set; }
        public string FLID { get; set; }
        public string NYID { get; set; }
        public string AIBD { get; set; }
        public string ICC { get; set; }
        public string AIA { get; set; }
        public string Phone { get; set; }
        public string Occupation { get; set; }
        public string State { get; set; }
        public string City { get; set; }

    }
}
